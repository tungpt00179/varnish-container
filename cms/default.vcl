vcl 4.0;

import directors;

backend backend1 {
    .host = "app-cms-vov.cms-vov.svc";
    .port = "8080";
        .first_byte_timeout = 60s;
        .connect_timeout = 3.5s;
        .between_bytes_timeout = 60s;
}

sub vcl_init {
        new backends = directors.round_robin();
        backends.add_backend(backend1);
}

sub vcl_recv {
        set req.backend_hint = backends.backend();
}

import std;

sub vcl_recv {
	
}

sub purge_regex {
    ban("obj.http.X-VC-Req-URL ~ " + req.url + " && obj.http.X-VC-Req-Host == " + req.http.host);
}

sub purge_exact {
    ban("obj.http.X-VC-Req-URL == " + req.url + " && obj.http.X-VC-Req-Host == " + req.http.host);
}

# Use the exact request URL, but ignore any query params
sub purge_page {
    set req.url = regsub(req.url, "\?.*$", "");
    ban("obj.http.X-VC-Req-URL-Base == " + req.url + " && obj.http.X-VC-Req-Host == " + req.http.host);
}

# The purge behavior can be controlled with the X-VC-Purge-Method header.
#
# Setting the X-VC-Purge-Method header to contain "regex" or "exact" will use
# those respective behaviors.  Any other value for the X-Purge header will
# use the default ("page") behavior.
#
# The X-VC-Purge-Method header is not case-sensitive.
#
# If no X-VC-Purge-Method header is set, the request url is inspected to attempt
# a best guess as to what purge behavior is expected.  This should work for
# most cases, although if you want to guarantee some behavior you should
# always set the X-VC-Purge-Method header.

sub vcl_recv {
    set req.http.X-VC-My-Purge-Key = "CMPYKaZvaPouWSLjrPV7YQJTvJyNMYiKGco6hmxEs7hEDr7ZxV8pJ3t8dPJyp3Sy";
    if (req.method == "PURGE" || req.method == "BAN") {

        if (req.http.X-VC-Purge-Key == req.http.X-VC-My-Purge-Key) {
            set req.http.X-VC-Purge-Key-Auth = "true";
        } else {
            set req.http.X-VC-Purge-Key-Auth = "false";
        }
        # Allow unrestricted purge from internal network.

        # If the header is set then it's an external request if we should check the purge key.
        if (req.http.X-Real-IP && req.http.X-VC-Purge-Key-Auth != "true") {
            return (synth(405, "Not allowed from " + client.ip));
        }
        # Usual authorization by the purge key.



        if (req.method == "BAN" && req.http.Cache-Tags) {
            ban("obj.http.Cache-Tags ~ " + req.http.Cache-Tags);
        }

        if (req.http.X-VC-Purge-Method) {
        }
    }
}


# The purge behavior can be controlled with the X-VC-Purge-Method header.
#
# Setting the X-VC-Purge-Method header to contain "regex" or "exact" will use
# those respective behaviors.  Any other value for the X-Purge header will
# use the default ("page") behavior.
#
# The X-VC-Purge-Method header is not case-sensitive.
#
# If no X-VC-Purge-Method header is set, the request url is inspected to attempt
# a best guess as to what purge behavior is expected.  This should work for
# most cases, although if you want to guarantee some behavior you should
# always set the X-VC-Purge-Method header.

sub vcl_recv {
    set req.http.X-VC-My-Purge-Key = "CMPYKaZvaPouWSLjrPV7YQJTvJyNMYiKGco6hmxEs7hEDr7ZxV8pJ3t8dPJyp3Sy";
    if (req.method == "PURGE" || req.method == "BAN") {

        if (req.http.X-VC-Purge-Key == req.http.X-VC-My-Purge-Key) {
            set req.http.X-VC-Purge-Key-Auth = "true";
        } else {
            set req.http.X-VC-Purge-Key-Auth = "false";
        }
        # Allow unrestricted purge from internal network.

        # If the header is set then it's an external request if we should check the purge key.
        if (req.http.X-Real-IP && req.http.X-VC-Purge-Key-Auth != "true") {
            return (synth(405, "Not allowed from " + client.ip));
        }
        # Usual authorization by the purge key.



        if (req.method == "BAN" && req.http.Cache-Tags) {
            ban("obj.http.Cache-Tags ~ " + req.http.Cache-Tags);
        }

        if (req.http.X-VC-Purge-Method) {
            if (req.http.X-VC-Purge-Method ~ "(?i)regex") {
                call purge_regex;
            } elsif (req.http.X-VC-Purge-Method ~ "(?i)exact") {
                call purge_exact;
            } else {
                call purge_page;
            }
        } else {
            # No X-VC-Purge-Method header was specified.
            # Do our best to figure out which one they want.
            if (req.url ~ "\.\*" || req.url ~ "^\^" || req.url ~ "\$$" || req.url ~ "\\[.?*+^$|()]") {
                call purge_regex;
            } elsif (req.url ~ "\?") {
                call purge_exact;
            } else {
                call purge_page;
            }
        }
        return (synth(200,"Purged " + req.url + " " + req.http.host));
    }
    unset req.http.X-VC-My-Purge-Key;
    # unset Varnish Caching custom headers from client
    unset req.http.X-VC-Cacheable;
    unset req.http.X-VC-Debug;
}

sub vcl_backend_response {
    # Bypass cache for files > N MB
    if (std.integer(beresp.http.Content-Length, 0) > 10485760) {
        set beresp.uncacheable = true;
        set beresp.ttl = 120s;
        return (deliver);
    }
}



sub vcl_recv {
    if (req.url ~ "(?i)\.(asc|doc|xls|ppt|csv|svg|jpg|jpeg|gif|png|ico|css|zip|tgz|gz|rar|bz2|pdf|txt|tar|wav|bmp|rtf|js|flv|swf|html|htm)(\?.*)?$") {

        # Do not use memory to cache static files.
        return (pass);

        set req.url = regsub(req.url, "\?.*$", "");
        # unset cookie only if no http auth
        if (!req.http.Authorization) {
            unset req.http.Cookie;
        }
        return(hash);
    }
}

sub vcl_backend_response {

}
 

# See https://book.varnish-software.com/4.0/chapters/VCL_Basics.html

sub vcl_recv {
    # Only cache GET and HEAD requests (pass through POST requests).
    if (req.method != "GET" && req.method != "HEAD") {
        set req.http.X-VC-Cacheable = "NO:Request method:" + req.method;
        return(pass);
    }

    # Implementing websocket support.
    if (req.http.Upgrade ~ "(?i)websocket") {
        return (pipe);
    }

    # Do not cache ajax requests.
    if (req.http.X-Requested-With == "XMLHttpRequest") {
        set req.http.X-VC-Cacheable = "NO:Requested with: XMLHttpRequest";
        return(pass);
    }

    # Strip hash, server does not need it.
    if (req.url ~ "\#") {
        set req.url = regsub(req.url, "\#.*$", "");
    }

    # Strip a trailing ? if it exists
    if (req.url ~ "\?$") {
        set req.url = regsub(req.url, "\?$", "");
    }


    set req.http.vckey = ";" + req.http.Cookie;
    set req.http.vckey = regsuball(req.http.vckey, "; +", ";");
    set req.http.vckey = regsuball(req.http.vckey, ";(VCKEY-[a-zA-Z0-9-_]+)=", "; \1=");
    set req.http.vckey = regsuball(req.http.vckey, ";[^ ][^;]*", "");
    set req.http.vckey = regsuball(req.http.vckey, "^[; ]+|[; ]+$", "");


    set req.http.Cookie = regsuball(req.http.Cookie, "(^|;\s*)(__[a-z]+|wooTracker|VCKEY-[a-zA-Z0-9-_]+)=[^;]*", "");
    set req.http.Cookie = regsub(req.http.Cookie, "^;\s*", "");
    if (req.http.Cookie ~ "^\s*$") {
        unset req.http.Cookie;
    }



    # Strip query parameters from all urls (so they cache as a single object).


    if (req.url ~ "(\?|&)(utm_[a-z]+|gclid|cx|ie|cof|siteurl|fbclid)=") {
        set req.url = regsuball(req.url, "&(utm_[a-z]+|gclid|cx|ie|cof|siteurl|fbclid)=([A-z0-9_\-\.%25]+)", "");
        set req.url = regsuball(req.url, "\?(utm_[a-z]+|gclid|cx|ie|cof|siteurl|fbclid)=([A-z0-9_\-\.%25]+)", "?");
        set req.url = regsub(req.url, "\?&", "?");
        set req.url = regsub(req.url, "\?$", "");
    }


}

sub vcl_recv {


    # Pass through any administrative or AJAX-related paths.
    if (req.url ~ "^(/update\\.php|/user/login|/([a-z]{2}/)?admin|/([a-z]{2}/)?admin/.*|/([a-z]{2}/)?system/files/.*|/([a-z]{2}/)?flag/.*|.*/ajax/.*|.*/ahah/.*)$") {
        return (pass);
    }

    if (req.url ~ "(^/([a-z]{2}/)?batch)") {
        return (pipe);
    }

    # Remove all cookies that Drupal doesn't need to know about. We explicitly
    # list the ones that Drupal does need, the SESS and NO_CACHE. If, after
    # running this code we find that either of these two cookies remains, we
    # will pass as the page cannot be cached.
    if (req.http.Cookie) {


        # 1. Append a semi-colon to the front of the cookie string.
        # 2. Remove all spaces that appear after semi-colons.
        # 3. Match the cookies we want to keep, adding the space we removed
        #    previously back. (\1) is first matching group in the regsuball.
        # 4. Remove all other cookies, identifying them by the fact that they have
        #    no space after the preceding semi-colon.
        # 5. Remove all spaces and semi-colons from the beginning and end of the
        #    cookie string.
        set req.http.Cookie = ";" + req.http.Cookie;
        set req.http.Cookie = regsuball(req.http.Cookie, "; +", ";");
        set req.http.Cookie = regsuball(req.http.Cookie, ";(SESS[a-z0-9]+|SSESS[a-z0-9]+|NO_CACHE)=", "; \1=");
        set req.http.Cookie = regsuball(req.http.Cookie, ";[^ ][^;]*", "");
        set req.http.Cookie = regsuball(req.http.Cookie, "^[; ]+|[; ]+$", "");

        if (req.http.Cookie == "") {
            # If there are no remaining cookies, remove the cookie header. If there
            # aren't any cookie headers, Varnish's default behavior will be to cache
            # the page.
            unset req.http.Cookie;
        }
        else {
            # If there is any cookies left (a session or NO_CACHE cookie), do not
            # cache the page. Pass it on to Apache directly.
            return (pass);
        }
    }
}

sub vcl_hash {
    set req.http.hash = req.url;
    if (req.http.host) {
        set req.http.hash = req.http.hash + "#" + req.http.host;
    } else {
        set req.http.hash = req.http.hash + "#" + server.ip;
    }

    //if (req.http.X-Forwarded-Proto ~ "https") {
        set req.http.hash = req.http.hash + req.http.X-Forwarded-Proto;
    //}

    if (req.http.vckey) {
        hash_data(req.http.vckey);
        unset req.http.vckey;
    }
}

sub vcl_pipe {
    # Called upon entering pipe mode.
    # In this mode, the request is passed on to the backend, and any further data from both the client
    # and backend is passed on unaltered until either end closes the connection. Basically, Varnish will
    # degrade into a simple TCP proxy, shuffling bytes back and forth. For a connection in pipe mode,
    # no other VCL subroutine will ever get called after vcl_pipe.

    # Note that only the first request to the backend will have
    # X-Forwarded-For set.  If you use X-Forwarded-For and want to
    # have it set for all requests, make sure to have:
    # set bereq.http.connection = "close";
    # here.  It is not set by default as it might break some broken web
    # applications, like IIS with NTLM authentication.

    # Implementing websocket support
    if (req.http.upgrade) {
        set bereq.http.upgrade = req.http.upgrade;
        set bereq.http.connection = req.http.connection;
    }



    return (pipe);
}

sub vcl_backend_response {
    set beresp.http.X-VC-Req-Host = bereq.http.host;
    set beresp.http.X-VC-Req-URL = bereq.url;
    set beresp.http.X-VC-Req-URL-Base = regsub(bereq.url, "\?.*$", "");

    if (beresp.grace < 2m) {
        set beresp.grace = 2m;
    }

    # Overwrite ttl with X-VC-TTL.
    if (beresp.http.X-VC-TTL) {
        set beresp.ttl = std.duration(beresp.http.X-VC-TTL + "s", 0s);
    }

    if (beresp.http.Set-Cookie) {
        set beresp.ttl = 0s;
    }

    if (bereq.http.X-VC-Cacheable ~ "^NO") {
        set beresp.http.X-VC-Cacheable = bereq.http.X-VC-Cacheable;
        set beresp.uncacheable = true;
        set beresp.ttl = 120s;

    } else if (beresp.ttl <= 0s) {
        if (!beresp.http.X-VC-Cacheable) {
            set beresp.http.X-VC-Cacheable = "NO:Not cacheable, ttl: "+ beresp.ttl;
        }
        set beresp.uncacheable = true;
        set beresp.ttl = 120s;

    } else if (beresp.http.Cache-Control ~ "private") {
        set beresp.http.X-VC-Cacheable = "NO:Cache-Control=private";
        set beresp.uncacheable = true;
        set beresp.ttl = 120s;

    } else if (beresp.http.Cache-Control ~ "no-cache") {
        set beresp.http.X-VC-Cacheable = "NO:Cache-Control=no-cache";
        set beresp.uncacheable = true;
        set beresp.ttl = 120s;

    } else if (beresp.http.X-VC-Enabled ~ "true") {
        if (!beresp.http.X-VC-Cacheable) {
            set beresp.http.X-VC-Cacheable = "YES:Is cacheable, ttl: " + beresp.ttl;
        }

    } else if (beresp.http.X-VC-Enabled ~ "false") {
        if (!beresp.http.X-VC-Cacheable) {
            set beresp.http.X-VC-Cacheable = "NO:Disabled";
        }
        set beresp.ttl = 0s;
    }

    if (beresp.status == 404 || beresp.status >= 500) {
        set beresp.ttl = 0s;
        set beresp.grace = 15s;
    }

    # Set ban-lurker friendly custom headers.
    set beresp.http.X-Url = bereq.url;
    set beresp.http.X-Host = bereq.http.host;

    if (beresp.http.Surrogate-Control ~ "BigPipe/1.0") {
        set beresp.do_stream = true;
        set beresp.ttl = 0s;
    }



    return(deliver);
}
sub vcl_backend_error {
  set beresp.http.Content-Type = "text/html; charset=utf-8";
  synthetic ({"
<html>
<head>
    <title>Not Found</title>
    <style type="text/css">
        A:link {text-decoration: none;  color: #333333;}
        A:visited {text-decoration: none; color: #333333;}
        A:active {text-decoration: none}
        A:hover {text-decoration: underline;}
    </style>
</head>
<body onload="setTimeout(function() { window.location.reload() }, 5000)" bgcolor=white text=#333333 style="padding:5px 15px 5px 15px; font-family: myriad-pro-1,myriad-pro-2,corbel,sans-serif;">
<H3>Page Unavailable</H3>
<p>The page you requested is temporarily unavailable.</p>
</body>
</html>
"});
  return (deliver);
}
sub vcl_deliver {
    unset resp.http.X-VC-Req-Host;
    unset resp.http.X-VC-Req-URL;
    unset resp.http.X-VC-Req-URL-Base;
    unset resp.http.Via;

    # Remove ban-lurker friendly custom headers when delivering to client.
    unset resp.http.X-Url;
    unset resp.http.X-Host;
    unset resp.http.Cache-Tags;

    if (obj.hits > 0) {
        set resp.http.X-VC-Cache = "HIT";
    } else {
        set resp.http.X-VC-Cache = "MISS";
    }

    if (req.http.X-VC-Debug ~ "true" || resp.http.X-VC-Debug ~ "true") {
        set resp.http.X-VC-Hash = req.http.hash;
        if (req.http.X-VC-DebugMessage) {
            set resp.http.X-VC-DebugMessage = req.http.X-VC-DebugMessage;
        }
    } else {
        unset resp.http.X-VC-Enabled;
        unset resp.http.X-VC-Debug;
        unset resp.http.X-VC-DebugMessage;
        unset resp.http.X-VC-Cacheable;
        unset resp.http.X-VC-Purge-Key-Auth;
        unset resp.http.X-VC-TTL;
    }
}